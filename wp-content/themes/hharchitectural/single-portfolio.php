<?php
get_header();
?>
<section class="container page-content" >
<hr class="vertical-space2">
<div class="col-md-8"><h1><?php the_title(); ?></h1></div>
<div class="col-md-4">
<?php if($webnus_options->webnus_portfolio_likebox_enable() == 1 ) { ?>
<div class="portfolio-item-dets">

<div class="portfolio-item-detail-box">
<span class="orange">LOCATION:</span> <?php echo types_render_field("location"); ?>
<hr>
<span class="orange">SECTOR:</span> <?php echo types_render_field("sector"); ?>
<hr>
<span class="orange">BUILD TYPE:</span> <?php echo types_render_field("build-type"); ?>
</div>
</div>
<?php } ?>
</div>
<article class="col-md-12">
<?php 
if(have_posts()): while(have_posts()): the_post();

echo the_content();

endwhile;?>
<div class="icon-box7 cta">
	<span>Call us now on 0121 559 6466</span>
	<p><a class="magicmore" href="<?php echo get_site_url(); ?>/contact">Get in touch</a></p>
</div>
<?php
endif;
if( '1' == $webnus_options->webnus_portfolio_recentworks_enable() )
	echo do_shortcode('[related_works count=7 title="Other Projects"]');
?>
</article>

</section>
  <?php get_footer(); ?>