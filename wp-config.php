<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hharchitectural');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'F+?u6w_f^7m?{rrB[#dA^e{A(Bx!Cf{^-Am#5xq+PddQ}KD>z^.5Kf,/DI`q}10/');
define('SECURE_AUTH_KEY',  '}hw-<(6%J<y_dMS+4@&alJ%B!-{-wI,ZBBHag,LL-@Q+9:n [98u,)%p4Z^HoHBN');
define('LOGGED_IN_KEY',    'hVb?5+4DGWu=3KzBG5&S8(unU!H:6} 94++4d|}^Xdbc*:*^7[ScpfUBaNP4^$vn');
define('NONCE_KEY',        '$I-NmG>@:K.iB`k,QlOl430f5:Pynp`=0a-R}~<-X3(dGj{%J,Axo&}vmrl{;qz1');
define('AUTH_SALT',        'sHq5H]k3LXh{Oc(r0e%|nJSlZB&pKGnuZjf~RA-M:v0K)[d[5F+-idVc1JUhv%mg');
define('SECURE_AUTH_SALT', '2=z_eZ);=#k`v&)bWzmTTby`t62.XkafwOU_|FH-<Nh=cUM$gJKp]A1=.T[q~dy:');
define('LOGGED_IN_SALT',   'F!vg]hDp!;*BMxz-SF)2XDSf|Qq{{&)[cfjL!pl)@6P%D #CJ%99JkvKc6@u6.xv');
define('NONCE_SALT',       '@BR;vKU#St#b?5-.)huJ|4zBWVw6%Bc:31[OwhZ,-Vd(igfstB,P/$*ScWL[@P H');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
